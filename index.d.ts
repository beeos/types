declare module '@beeos/program' {

  const execute: (main: (call: KernelCall, connection?: IEstablishStdConnection, args?: string[]) => Promise<IProcess>) => void;

  type KernelCall = (code: kernel_call, ...args: any[]) => any;

  type kernel_call = 
  'draw-to-screen' |
  'fs-read-file' |
  'fs-read-dir' |
  'fs-make-directory' |
  'fs-exists' |
  'execute-program' |
  'add-keyboard-listener' |
  'get-keyboard-state';

  interface IEstablishStdConnection {
    establishStdIn: (stdIn: Pipe) => void;
    establishStdOut: () => Pipe;
  }

  interface IProgram {
    pid: number;
    run(call: KernelCall, connection?: IEstablishStdConnection, args?: string[]): Promise<IProcess>;
    pipes?: IStdIO;
  }

  type Pipe = (message: string) => void;

  interface IProcess {
    exitCode: number;
  }

  interface IStdIO {
    in: Pipe[];
    out: Pipe[];
  }

  export {KernelCall, kernel_call, IEstablishStdConnection, Pipe, IProcess, IStdIO, execute};
}

declare const execute: (main: any) => void;
